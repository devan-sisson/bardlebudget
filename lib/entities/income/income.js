const { struct } = require('superstruct')

module.exports.Income = struct({
  id: 'number?',
  timeStamp: 'date?',
  name: 'string',
  plannedAmount: 'number',
  actualAmount: 'number?',
  frequency: 'string'
})
